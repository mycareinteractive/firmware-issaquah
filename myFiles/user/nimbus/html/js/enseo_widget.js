/**
 * @fileOverview HD2000 Nimbus Widget Handler Javascript API
 * <p><b>Copyright (c) Enseo, Inc. 2008
 *
 * This material contains trade secrets and proprietary information
 * and	may not	 be used, copied, disclosed or	distributed	 except
 * pursuant to	a license from Enseo, Inc. Use of copyright notice
 * does not imply publication or disclosure.</b>
 */

////////////////////////////////////////////////////////////////////////
//
// Reference   : $Source: /cvsroot/calica/apps/rootfs_common/root/usr/bin/data/html/js/enseo_widget.js,v $
//
// Revision	   : $Revision: 1.1 $
//
// Date		   : $Date: 2009-08-31 17:06:08 $
//
// Author(s)   : Tom Miller
//
// Description : HD2000 Nimbus Widget Handler javascript API.
//
////////////////////////////////////////////////////////////////////////

var g_SelfRef = null;

function EnseoWidget(Name, InitiallyVisible)
{
	this.NimbusSelf = Nimbus.getBrowserWidgetSelf(Name);
	this.ShowState = InitiallyVisible ? true : false;

	this.NimbusSelf.setVisibility(this.ShowState);

	this.ShowCode = -1;
	this.HideCode = -1;
	this.HideTimeout = -1;
	this.ActivityTimer = null;

	g_SelfRef = this;
}

EnseoWidget.prototype.constructor = EnseoWidget;

EnseoWidget.prototype.HandleMessage = function(Msg)
{
	var SubStrings = new Array();

    SubStrings = Msg.split(",");

	switch (SubStrings[0])
	{
		case "Enseo.Show":
			return this.OnShow();

		case "Enseo.Hide":
			return this.OnHide();

		case "Enseo.SetField":
			return this.OnSetField(SubStrings[1], SubStrings[2]);

		default:
			return false;
	}

	return false;
}

EnseoWidget.prototype.OnShow = function()
{
	if (window['OnEnseoWidgetShow'])
	{
		return OnEnseoWidgetShow();
	}
	return this.Show();
}

EnseoWidget.prototype.Show = function()
{
	if (null == this.NimbusSelf) return false;
	this.NimbusSelf.setVisibility(true);
	this.NimbusSelf.raiseToTop();
	this.NimbusSelf.setFocus();

	this.ShowState = true;

	this.MarkActivity();

	return true;
}

EnseoWidget.prototype.OnHide = function()
{
	if (window['OnEnseoWidgetHide'])
	{
		return OnEnseoWidgetHide();
	}
	return this.Hide();
}


EnseoWidget.prototype.Hide = function()
{
	if (null == this.NimbusSelf) return false;

	this.NimbusSelf.setVisibility(false);
	this.NimbusSelf.lowerToBottom();
	this.NimbusSelf.removeFocus();

	this.ShowState = false;

	return true;
}

EnseoWidget.prototype.OnSetField = function(Parm, Value)
{
	if (window['OnEnseoWidgetSetField'])
	{
		return OnEnseoWidgetSetField(Parm, Value);
	}
	return this.SetField(Parm, Value);
}

EnseoWidget.prototype.SetField = function (Parm, Value)
{
	switch (Parm)
	{
		case "ShowCode":
			this.ShowCode = parseInt(Value);
			if (-1 == this.HideCode)
			{
				this.HideCode = this.ShowCode;
			}
			break;

		case "HideCode":
			this.HideCode = parseInt(Value);
			break;

		case "HideTimeout":
			this.HideTimeout = parseInt(Value);
			this.MarkActivity();
			break;

		default:
			return false;
	}

	return true;
}

EnseoWidget.prototype.HandleCommand = function(cmdCode)
{
	if (this.ShowCode == cmdCode ||
		this.HideCode == cmdCode)
	{
		if (this.ShowCode == cmdCode &&
			this.HideCode != cmdCode)
		{
			this.OnShow();
		} else if (
			this.ShowCode != cmdCode &&
	   		this.HideCode == cmdCode) 
		{
			this.OnHide();
		}
		else if (this.ShowState)
		{
			this.OnHide();
		} else	{
			this.OnShow();
		}
		
		return true;
	}

	return false;
}

function EndeoWidgetInActive()
{
	g_SelfRef.ActivityTimer = null;
	g_SelfRef.Hide();
}

EnseoWidget.prototype.CancelActivityTimer = function()
{
	if (this.ActivityTimer) 
	{
		var tmp = this.ActivityTimer;
		this.ActivityTimer = null;

		clearTimeout(tmp);		
	}
}

EnseoWidget.prototype.MarkActivity = function()
{
	if (this.ShowState && ( -1 != this.HideTimeout))
	{
		this.CancelActivityTimer();
		this.ActivityTimer = setTimeout('EndeoWidgetInActive()', this.HideTimeout);
	}
}

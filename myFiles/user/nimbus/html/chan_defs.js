//////////////////////////////////////////////////////////////////////////////////////////
//	Enseo Nimbus API usage example
//	Channel definitions
//	Copyright (c) Enseo, Inc. 1997-2006
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
// Code for building the channel list used by the Nimbus example application
//////////////////////////////////////////////////////////////////////////////////////////

// Array of available channels
var Chan_Table = new Array();
	
function BuildChanRF(freq, program, chn, label, audio_video) {
    var chanobj = new Object;
	chanobj.type = "rf";
	chanobj.ID = chn;			          	//channel number
	chanobj.program = program;	          	//program	number
	chanobj.freq = freq;			      	//frequency
	chanobj.label = label;				  	//label
	chanobj.audio_video = audio_video;	  	//A/V usage type
    return chanobj;																						
}

function BuildChanIP(ip, port, chn, label, audio_video) {
    var chanobj = new Object;
	chanobj.type = "ip";
	chanobj.ID = chn;			          	//channel number
	chanobj.ip = ip;						//IP address
	chanobj.port = port						//port
	chanobj.label = label;				  	//label
	chanobj.audio_video = audio_video;	  	//A/V usage type
    return chanobj;																						
}

var idx = 0;
//                                 freq	   prog   ID	label  usage: A = audio,  AV = audio and video
//Chan_Table[idx++] = BuildChanRF( "657000", "1", "02", "CNN", "AV"); 
//                              IP            		port  	  	ID	  	label                		usage: A = audio, AV = audio and video
//QAM only LAuberge
//
Chan_Table[idx++] = BuildChanRF( "633000", "1", "03", ".KATC - ABC", "AV"); 
Chan_Table[idx++] = BuildChanRF( "861000", "4", "04", ".QVC", "AV"); 
Chan_Table[idx++] = BuildChanRF( "633000", "2", "05", ".Government", "AV"); 
Chan_Table[idx++] = BuildChanRF( "633000", "3", "06", ".KFDM - CBS", "AV"); 
Chan_Table[idx++] = BuildChanRF( "627000", "3", "07", ".KVHP-CW", "AV"); 
//
Chan_Table[idx++] = BuildChanRF( "831000", "1", "09", ".KPLC-NBC HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "831000", "2", "10", ".KLFY-CBS HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "837000", "1", "11", ".KVHP - FOX HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "849000", "1", "12", ".KBMT - ABC HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "837000", "2", "13", ".KLTL-TBS HD", "AV"); 
//
Chan_Table[idx++] = BuildChanRF( "843000", "1", "15", ".TBS HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "861000", "6", "16", ".EWTN", "AV"); 
Chan_Table[idx++] = BuildChanRF( "639000", "1", "17", ".Weather Channel", "AV"); 
//
Chan_Table[idx++] = BuildChanRF( "627000", "2", "19", ".Inspirational", "AV"); 
Chan_Table[idx++] = BuildChanRF( "633000", "4", "20", ".KFAM - LP", "AV"); 
//
Chan_Table[idx++] = BuildChanRF( "795000", "1", "22", ".BET HD", "AV"); 
//
Chan_Table[idx++] = BuildChanRF( "753000", "3", "24", ".Disney HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "753000", "1", "25", ".USA HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "807000", "1", "26", ".AMC", "AV"); 
Chan_Table[idx++] = BuildChanRF( "843000", "2", "27", ".Fox Sports Southwest HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "789000", "2", "28", ".MTV HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "771000", "2", "29", ".CNN HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "777000", "1", "30", ".Lifetime HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "783000", "2", "31", ".Nickelodeon HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "771000", "3", "32", ".Animal Planet HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "765000", "1", "33", ".TLC HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "777000", "2", "34", ".Cartoon Network HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "825000", "1", "35", ".ESPN HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "825000", "2", "36", ".ESPN 2 HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "753000", "2", "37", ".FX HD", "AV"); 
Chan_Table[idx++] = BuildChanRF( "783000", "1", "38", ".Spike HD", "AV"); 
//
//Chan_Table[idx++] = BuildChanIP("239.79.200.115",   "20000",    "002",   ".The CW HD",    "AV");
// 32 fashion tv?
///////////////////////////////////////////////////////////////////////////////////////////////////
var MaxChans = Chan_Table.length;
var DefChannel = 0;

// Array of available channels30
var ChanList = new Array();

// Build a channel list object
function BuildChannel(Label, ChanDesc) {
	var ChanObj = new Object();
	ChanObj.ChanDesc = ChanDesc;
	ChanObj.Label = Label;
	return ChanObj;
}

// Build Chan_Table list
for (i=0; i < MaxChans; i++) {
	var ChanUsage = "AudioVideo";
	if (Chan_Table[i].audio_video == "A") {
		ChanUsage = "AudioOnly";
	}
	if (Chan_Table[i].type == "rf") {
		ChanList.push(BuildChannel('"' + Chan_Table[i].label + '"', 
										'<ChannelParams ChannelType="Digital" \
											ChannelUsage="' + ChanUsage + '"> \
											<DigitalChannelParams \
												PhysicalChannelIDType="Freq" \
												PhysicalChannelID="' + Chan_Table[i].freq + '" \
												DemodMode="QAM256" \
												ProgramSelectionMode="PATProgram" \
												ProgramID="' + Chan_Table[i].program + '"> \
											</DigitalChannelParams> \
										</ChannelParams>'));
	} else if (Chan_Table[i].type == "ip") {
		ChanList.push(BuildChannel('"' + Chan_Table[i].label + '"', 
										'<ChannelParams ChannelType="UDP" \
											Encryption="Proidiom" \
											ChannelUsage="' + ChanUsage + '"> \
											<UDPChannelParams \
												Address="' + Chan_Table[i].ip + '" \
												Port="' + Chan_Table[i].port + '"> \
											</UDPChannelParams> \
										</ChannelParams>'));
	}													
	Nimbus.logMessage("ChanList : " + ChanList[i].ChanDesc + "   " +ChanList[i].Label);
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////


#!/bin/bash

shopt -s nullglob

for f in *.eoc; do
  echo "Appending user content to $f..."
  
  parts=(${f//_/ })
  
  output=${f/#prod/aceso}
  platform="HD3100"
  newmem="_NewMem"
  
  output=${output/stbwb/issq_$BUILD_NUMBER}
  
  case ${parts[5]} in
    "7405") platform="HD3000";;
    "7231") platform="HD3100";;
  esac

  if [ ${parts[1]} -lt 2 ] || [ ${parts[2]} -lt 15 ] || [[ $output == *newmem_transition* ]]; then
    newmem=""
  fi
  
  eoappendsfdi "$f" myFiles $output $platform$newmem --splash=splash.bmp
done
